#include <iostream>
#include <fstream>
#include "Arguments.h"
#include "hdump_functions.h"

using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;

int main (int argc, char* argv[])
{
	Arguments arguments(argc, argv);
	if (arguments)
		return 0;
	ifstream is(arguments.name);
	if (!is)
	{
		cerr << "EE: Opening of " << arguments.name << " failed!" << endl;
		return 1;
	}
	if (!print(is))
	{
		return 2;
	}
}

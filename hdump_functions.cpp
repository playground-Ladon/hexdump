#include <iostream>
#include <sys/ioctl.h>
#include <sys/termios.h>
#include <unistd.h>
#include "Colorize.h"
#include "hdump_functions.h"

using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;

int columns ()
{
	struct winsize ws;
	ioctl (STDOUT_FILENO, TIOCGWINSZ, &ws);
	if (Colorize::mode == Mode::HEX)
	{
		return ws.ws_col / 2;
	}
	else
	{
		return ws.ws_col;
	}
}

bool print (ifstream& is)
{
	int cols = columns();
	for (int i = is.get(); is.good(); i = is.get())
	{
		cout << Colorize(i).operator const char*();
		if (! (is.tellg() % cols))
		{
			cout << endl;
		}
	}
	if (is.fail() and !is.eof())
	{
		cerr << "EE: Error reading file!" << endl << endl;
		return true;
	}
	else
	{
		cout << endl;
		return false;
	}
}

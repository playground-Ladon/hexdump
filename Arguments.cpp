#include <cstring>
#include <iostream>
#include "Arguments.h"
#include "Colorize.h"

using std::cerr;
using std::cout;
using std::endl;
using std::strlen;


Arguments::Arguments (const int& argc, const char* const argv[]) : name(nullptr), state(false)
{
	if (!parse_name(argc, argv))
	{
		state = true;
		cerr << "No filename found!" << endl;
	}
	parse_options(argc, argv);
}

Arguments::operator bool ()
{
	return state;
}

bool Arguments::parse_name (const int& argc, const char* const argv[])
{
	for (int i = 1; i < argc; ++i)
	{
		// Name found!
		// -
		if (argv[i][0] != '-')
		{
			if (!name)
			{
				name = argv[i];
			}
			else
			{
				// Ensure single input.
				// -
				cerr << "EE: Multiple input filenames specified!!" << endl;
				state = true;
			}
		}
	}
	if (name)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Arguments::parse_options (const int& argc, const char* const argv[])
{
	for (int i = 1; i < argc; ++i)
	{
		// Option found!
		// -
		if (argv[i][0] == '-')
		{
			for (size_t j = 0; j < strlen(argv[i]); ++j)
			{
				switch (argv[i][j])
				{
				case 'a': case 'A':
					Colorize::mode = Mode::ASCII;
					break;
				case 'h': case 'H':
					print_help();
					state = true;
					break;
				case 'n': case 'N':
					if (Colorize::mode == Mode::ASCII)
					{
						Colorize::mode = Mode::ASCII_NO_WS;
					}
					break;
				case 'x': case 'X':
					Colorize::mode = Mode::HEX;
					break;
				}
			}
		}
	}
	return true;
}

// Help message.
// -
void Arguments::print_help ()
{
	cout << "Usage: hdump [FILE]" << endl
		 << "  -h, -H	print help" << endl
		 << "  -x, -X	print in hex format" << endl
		 << "  -a, -A	print only ASCII characters" << endl
		 << "  -n, -N	with -a, do not print whitespace" << endl
		 // NOTE: add UTF-?? support.
		 // -
		 << endl;
}

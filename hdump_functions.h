#include <fstream>

// Line width.
// -
int columns ();

// Hex dump.
// -
bool print (std::ifstream& is);

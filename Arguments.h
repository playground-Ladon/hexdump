#ifndef ARGUMENTS_H
#define ARGUMENTS_H

struct Arguments
{
	explicit Arguments (const int& argc, const char* const argv[]);
		// 2nd argument, leftmost const, refers to
		// the innermost dimension argv[][] whereas
		// the rightmost to the outer one argv[].
		// -
	explicit operator bool ();
	const char* name;

private:

	bool parse_name (const int& argc, const char* const argv[]);

	// Options:
	// -h, -H   -> Print help.
	// -x		-> Print hex form.
	// -a		-> Print ASCII form.
	// -ax, -xa -> Allowed. Last should be stored.
	// -n, -N   -> Used with -a. Do not print <newline> and whitespaces except for <space>.
	// -
	bool parse_options (const int& argc, const char* const argv[]);

	void print_help ();

	// False is good!
	// -
	bool state;
};

#endif

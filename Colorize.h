#ifndef COLORIZE_H
#define COLORIZE_H

#include <string>

enum class Mode {HEX, ASCII, ASCII_NO_WS};

struct Colorize
{
public:
	explicit Colorize (const int& i, const Mode& mmode, const bool& wwhitespace);
	explicit Colorize (const int& i, const Mode& mmode);
	explicit Colorize (const int& i);

	// The following is an alternative to defining operator <<.
	// -
	// Note: Find a way to keep operator explicit without the need to call it, explicitly.
	// Bad example: Colorize(..).operator const char*().
	// -
	explicit operator const char* () const;

	static Mode mode;
private:
	void transform (const int& i);
	std::string text;
};

#endif

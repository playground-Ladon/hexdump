CC      = g++
# Disabling -march=native -mtune=native -Ofast because i got segmentation fault.
# -
FLAGS   = -fdiagnostics-color=always -g -pipe -std=c++2a -Wall -Wextra
MAIN    = hdump
HEADERS = Arguments.h Colorize.h $(MAIN)_functions.h
OBJ     = Arguments.o Colorize.o $(MAIN).o $(MAIN)_functions.o
GEN     = $(MAIN) *.o *.orig *.gch heaptrack.*

# Hack to clear before action.
# -
all: clear $(MAIN)

clear:
	@clear

# Meaning of '$^', below, is, 'first of list'.
# Meaning of '$@' is, 'target'.
# -
$(MAIN): $(OBJ)
	@echo "Linking $@"
	@$(CC) -o $@ $^

$(MAIN).o: $(MAIN).cpp $(HEADERS)
	@echo "Compiling $@"
	@$(CC) $(FLAGS) -c $<

# Chrono.o: Chrono.cc Chrono.h
# 	$(CC) $(FLAGS) -c Chrono.cc

# Meaning of '$<', below, is, 'full list'.
# Meaning of '$@' is, 'target'.
# -
%.o: %.cpp %.h
	@echo "Compiling $@"
	@$(CC) $(FLAGS) -o $@ -c $<

clean:
	@rm -rvf $(MAIN) $(GEN)

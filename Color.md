## Console colorization options.
There are at least two ways:
* Choosing distinct colors in some range 0..7 (***30***..***37***)
  * *\1b[**37**m* For white text. *\1b[0m* To reset.
  * ***47***... for background.
* Setting the RGB color:
  * *\1b[**38;2**;255;255;0m* Yellow text. *\1b[0m*
  * ***48;2*** for background...

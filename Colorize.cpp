#include <iomanip>
#include <sstream>
#include <string>
#include "Colorize.h"

using std::hex;
using std::ostringstream;
using std::setfill;
using std::setw;

Mode Colorize::mode = Mode::HEX;

Colorize::Colorize (const int& i, const Mode& mmode)
{
	mode = mmode;
	transform(i);
}

Colorize::Colorize (const int& i)
{
	transform(i);
}

Colorize::operator const char* () const
{
	return text.c_str();
}

void Colorize::transform (const int& i)
{
	ostringstream ss;
	ss << "\x1b[";

	if (!i)
	{
		ss << "38;2;0;0;0m";
	}
	else if (isprint(i))
	{
		ss << "38;2;" << i << ";" << i << ";0;48;2;0;0;" << i << "m";
	}
	else if (isspace(i))
	{
		ss << "48;2;64;64;0m";
	}
	else
	{
		ss << "38;2;255;0;0m";
	}

	if (mode == Mode::HEX)
	{
		ss << setw(2) << setfill('0') << hex << i << "\x1b[0m";
	}
	else if (mode == Mode::ASCII or mode == Mode::ASCII_NO_WS)
	{
		if (isprint(i) or (isspace(i) and mode == Mode::ASCII))
		{
			ss << static_cast<char>(i) << "\x1b[0m";
		}
		else
		{
			ss << " \x1b[0m";
		}
	}

	text = ss.str();
}
